# Simple Macau Simulator

Simple simulator for Macau card game written fully in TypeScript.

## Prerequisites

In order to install, build and run this project, you will need [Node.js](https://nodejs.org/en/) (tested for v14.9.0).

## Installation & build

### Installation

To install the package, simply run the `npm install` command.

### Build

To build the files run `npm run build` command.

To build and watch files for changes run `npm run watch` command.
