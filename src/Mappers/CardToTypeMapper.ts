import Card from "../Classes/Card/Card";
import CardType from "../Enums/CardType";
import Suit from "../Enums/Suit";

class CardToTypeMapper
{
    /**
     * Maps the card to card type.
     * Each card type provides other game state when played.
     *
     * @param {Card} card
     * @return {CardType}
     */
    public static mapCardToType(card: Card): CardType
    {
        const rank = card.getRank();
        const suit = card.getSuit();

        // Twos and threes of any suit as well as King of Spades and King of Hearts are the combat cards
        if (rank === 2 || rank === 3 || (rank === 13 && (suit === Suit.Spades || suit === Suit.Hearts))) {
            return CardType.Combat;
        }

        // Fours are turn-skipping cards
        else if (rank === 4) {
            return CardType.TurnSkipping;
        }

        // Jacks are rank-demanding cards
        else if (rank === 11) {
            return CardType.RankDemanding;
        }

        // Aces are suit-changing cards
        else if (rank === 14) {
            return CardType.SuitChanging;
        }

        // All other cards are just regular cards
        return CardType.Regular;
    }
}

export default CardToTypeMapper;
