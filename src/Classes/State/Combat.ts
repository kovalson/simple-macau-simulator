import CardType from "../../Enums/CardType";
import Card from "../Card/Card";
import CardCollection from "../Collection/CardCollection";
import Game from "../Game";
import Player from "../Player";
import GameState from "./GameState";
import Regular from "./Regular";

/**
 * Whomever loses the combat state, they need to draw as many cards from the stack,
 * as much value the state holds. The state value is denoted by the combat cards' ranks.
 */
class Combat extends GameState
{
    /**
     * The total amount of cards to draw upon losing the combat.
     *
     * @protected
     * @type {number}
     */
    protected cardsToDraw: number;

    /**
     * Creates a new combat game state.
     *
     * @param {Game} game
     * @param {number} cardsToDraw
     */
    constructor(game: Game, cardsToDraw: number)
    {
        super(game);

        this.cardsToDraw = cardsToDraw;
    }

    /**
     * @inheritDoc
     */
    public isCardPlayable(card: Card): boolean
    {
        return (card.getType() === CardType.Combat);
    }

    /**
     * @inheritDoc
     */
    public handlePlayerTurn(): void
    {
        const currentPlayer: Player = this.getCurrentPlayer();

        if (this.tryToPlayCards()) {
            return;
        }

        while (this.cardsToDraw--) {
            let card: Card | undefined = this.drawCardFromDeck();

            if (card) {
                currentPlayer.getCards().add(card);
            } else {
                this.game.restockDeck();

                card = this.drawCardFromDeck();

                if (card) {
                    currentPlayer.getCards().add(card);
                } else {
                    break;
                }
            }
        }

        this.setNextState(Regular);
    }

    /**
     * @inheritDoc
     */
    protected onSuccessfulTry(cards: CardCollection): void
    {
        this.cardsToDraw += cards.getValuesSum();
    }

    /**
     * Increases the total number of cards to draw by given amount.
     *
     * @param {number} amount
     * @return {void}
     */
    public increaseCardsToDraw(amount: number): void
    {
        this.cardsToDraw += amount;
    }
}

export default Combat;
