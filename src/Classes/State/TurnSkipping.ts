import CardType from "../../Enums/CardType";
import Card from "../Card/Card";
import Game from "../Game";
import Player from "../Player";
import GameState from "./GameState";
import Regular from "./Regular";

class TurnSkipping extends GameState
{
    /**
     * The total number of turns to skip upon losing the state fight.
     *
     * @protected
     * @type {number}
     */
    protected turnsToSkip: number;

    /**
     * Creates a new combat game state.
     *
     * @param {Game} game
     * @param {number} turnsToSkip
     */
    constructor(game: Game, turnsToSkip: number)
    {
        super(game);

        this.turnsToSkip = turnsToSkip;
    }

    /**
     * @inheritDoc
     */
    public isCardPlayable(card: Card): boolean
    {
        return (card.getType() === CardType.TurnSkipping);
    }

    /**
     * @inheritDoc
     */
    public handlePlayerTurn(): void
    {
        const currentPlayer: Player = this.getCurrentPlayer();

        if (currentPlayer.skipsTurn()) {
            currentPlayer.decreaseTurnsToSkip();
            return;
        }

        const cardsToPlay = currentPlayer.getCardsToPlay(this);

        if (cardsToPlay.hasItems()) {
            this.playCards(cardsToPlay);
            this.increaseTurnsToSkip(cardsToPlay.count());
            return;
        }

        currentPlayer.setTurnsToSkip(this.turnsToSkip);

        this.setNextState(Regular);
    }

    /**
     * Increases the total number of turns to skip by given amount.
     *
     * @param {number} amount
     * @return {void}
     */
    public increaseTurnsToSkip(amount: number): void
    {
        this.turnsToSkip += amount;
    }
}

export default TurnSkipping;
