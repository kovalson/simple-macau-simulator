import GameStateFactory from "../../Factories/GameStateFactory";
import Card from "../Card/Card";
import CardCollection from "../Collection/CardCollection";
import GameState from "./GameState";

/**
 * The regular game state.
 * During this state there is no combat and everyone can play any card
 * that has the same rank or suit as the stack top card.
 */
class Regular extends GameState
{
    /**
     * @inheritDoc
     */
    public isCardPlayable(card: Card): boolean
    {
        // We peek the top card of the game stack
        const stackTopCard: Card = this
            .game
            .getStack()
            .peek();

        return (
            card.getRank() === stackTopCard.getRank() ||
            card.getSuit() === stackTopCard.getSuit()
        );
    }

    /**
     * @inheritDoc
     */
    public handlePlayerTurn(): void
    {
        this.tryToPlayCards();
    }

    /**
     * @inheritDoc
     */
    protected onSuccessfulTry(cards: CardCollection): void
    {
        this.game.setState(
            GameStateFactory.getStateFromCardsPlayed(this.game, cards)
        );
    }
}

export default Regular;
