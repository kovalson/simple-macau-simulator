import Suit from "../../Enums/Suit";
import GameStateFactory from "../../Factories/GameStateFactory";
import Card from "../Card/Card";
import CardCollection from "../Collection/CardCollection";
import Game from "../Game";
import GameState from "./GameState";

class SuitChanging extends GameState
{
    /**
     * The new suit.
     *
     * @protected
     * @type {Suit}
     */
    protected newSuit: Suit;

    /**
     * Creates a new suit-changing state.
     *
     * @param {Game} game
     * @param {Suit} newSuit
     */
    public constructor(game: Game, newSuit: Suit)
    {
        super(game);

        this.newSuit = newSuit;
    }

    /**
     * @inheritDoc
     */
    public isCardPlayable(card: Card): boolean
    {
        // We peek at the top stack card
        const topStackCard: Card = this.peekTopStackCard();

        return (
            card.getSuit() === this.newSuit ||
            card.getRank() === topStackCard.getRank()
        );
    }

    /**
     * @inheritDoc
     */
    public handlePlayerTurn(): void
    {
        this.tryToPlayCards();
    }

    /**
     * @inheritDoc
     */
    protected onSuccessfulTry(cards: CardCollection): void
    {
        this.resetCardsProperties();

        this.game.setState(
            GameStateFactory.getStateFromCardsPlayed(this.game, cards)
        );
    }
}

export default SuitChanging;
