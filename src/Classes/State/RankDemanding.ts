import CardType from "../../Enums/CardType";
import Rank from "../../Types/Rank";
import Card from "../Card/Card";
import RankDemandingCard from "../Card/RankDemandingCard";
import CardCollection from "../Collection/CardCollection";
import Game from "../Game";
import GameState from "./GameState";
import Regular from "./Regular";

class RankDemanding extends GameState
{
    /**
     * The rank demanded.
     *
     * @protected
     * @type {Rank}
     */
    protected rankDemanded: Rank;

    /**
     * The number of turns left to automatically leave this state.
     *
     * @protected
     * @type {number}
     */
    protected turnsLeft: number;

    /**
     * Creates a new combat game state.
     *
     * @param {Game} game
     * @param {Rank} rankDemanded
     */
    constructor(game: Game, rankDemanded: Rank)
    {
        super(game);

        this.rankDemanded = rankDemanded;
        this.turnsLeft = game
            .getPlayers()
            .getActivePlayers()
            .count();
    }

    /**
     * @inheritDoc
     */
    public isCardPlayable(card: Card): boolean
    {
        // We peek at the top stack card
        const topStackCard = this
            .game
            .getStack()
            .peek();

        return (
            card.getRank() === this.rankDemanded ||
            (card.getType() === CardType.RankDemanding && card.getSuit() === topStackCard.getSuit())
        );
    }

    /**
     * @inheritDoc
     */
    public handlePlayerTurn(): void
    {
        this.tryToPlayCards();

        this.update();
    }

    /**
     * @inheritDoc
     */
    protected onSuccessfulTry(cards: CardCollection): void
    {
        if (cards.getCardsType() === CardType.RankDemanding) {
            this.rankDemanded = (cards.first()! as RankDemandingCard).getRankDemanded();
        }
    }

    /**
     * @override
     * @inheritDoc
     */
    public update(): void
    {
        if (--this.turnsLeft <= 0) {
            this.resetCardsProperties();
            this.setNextState(Regular);
        }
    }
}

export default RankDemanding;
