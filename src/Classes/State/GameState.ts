import Card from "../Card/Card";
import CardCollection from "../Collection/CardCollection";
import Game from "../Game";
import Logger from "../Logger";
import Player from "../Player";

abstract class GameState
{
    /**
     * The game reference.
     *
     * @protected
     * @type {Game}
     */
    protected game: Game;

    /**
     * Creates new game state.
     *
     * @protected
     * @param {Game} game
     */
    public constructor(game: Game)
    {
        this.game = game;
    }

    /**
     * Handles the player's turn.
     *
     * @return {void}
     */
    abstract handlePlayerTurn(): void;

    /**
     * Checks whether given card is playable in current state.
     *
     * @param {Card} card
     * @return {boolean}
     */
    abstract isCardPlayable(card: Card): boolean;

    /**
     * Updates the state.
     *
     * @return {void}
     */
    public update(): void
    {
        // Do nothing by default
    }

    /**
     * Returns the current player.
     *
     * @return {Player}
     */
    public getCurrentPlayer(): Player
    {
        return this
            .game
            .getPlayers()
            .getCurrentPlayer();
    }

    /**
     * Draws a card from the game deck.
     *
     * @return {Card | undefined}
     */
    public drawCardFromDeck(): Card | undefined
    {
        return this
            .game
            .getDeck()
            .draw();
    }

    /**
     * Peeks the card from the top of the game stack.
     *
     * @return {Card}
     */
    public peekTopStackCard(): Card
    {
        return this
            .game
            .getStack()
            .peek();
    }

    /**
     * Puts given cards onto the stack.
     *
     * @param {CardCollection} cards
     * @return {void}
     */
    public playCards(cards: CardCollection): void
    {
        this.game
            .getStack()
            .add(cards);
    }

    /**
     * Sets the new game state.
     *
     * @param {{new(args: Array): GameState}} GameStateClass
     * @param {Array} args
     * @return {void}
     */
    public setNextState(GameStateClass: new (...args: Array<any>) => GameState, ...args: Array<any>): void
    {
        this.game.setState(
            new GameStateClass(this.game, ...args)
        );
    }

    /**
     * Checks whether the state is of given class.
     *
     * @template T
     * @param {{new(args: Array): T}} type
     * @return {boolean}
     */
    public isType<T extends GameState>(type: new (...args: Array<any>) => T): boolean
    {
        return (this instanceof type);
    }

    /**
     * Tries to play current player's cards. If failed, draws a single card
     * from the deck and retries the process. On successful try calls the
     * onSuccessfulTry() method and return true. On failure returns false.
     *
     * @see onSuccessfulTry()
     * @protected
     * @return {boolean}
     */
    protected tryToPlayCards(): boolean
    {
        const currentPlayer: Player = this.getCurrentPlayer();

        Logger.log("Current game state is", this.constructor.name);

        if (currentPlayer.skipsTurn()) {
            currentPlayer.decreaseTurnsToSkip();
            return false;
        }

        let cardsToPlay: CardCollection = currentPlayer.getCardsToPlay(this);

        if (cardsToPlay.hasItems()) {
            Logger.log(currentPlayer.getName(), "plays", cardsToPlay.toString());
            this.playCards(cardsToPlay);
            this.onSuccessfulTry(cardsToPlay)

            return false;
        }

        const topCard: Card | undefined = this.game.drawCardFromDeck();

        if (topCard) {
            currentPlayer.getCards().add(topCard);
            cardsToPlay = currentPlayer.getCardsToPlay(this);
            Logger.log(currentPlayer.getName(), "draws", topCard.toString());

            if (cardsToPlay.hasItems()) {
                Logger.log(currentPlayer.getName(), "plays", cardsToPlay.toString());
                this.playCards(cardsToPlay);
                this.onSuccessfulTry(cardsToPlay);

                return true;
            }
        }

        return false;
    }

    /**
     * Runs the callback after successfully playing cards.
     *
     * @see tryToPlayCards()
     * @protected
     * @param {CardCollection} cards
     * @return {void}
     */
    protected onSuccessfulTry(cards: CardCollection): void
    {
        // Do nothing by default
    }

    /**
     * Resets the special properties of cards in the game.
     *
     * @protected
     * @return {void}
     */
    protected resetCardsProperties(): void
    {
        this.game
            .getStack()
            .map((card: Card) => card.reset());

        this.game
            .getDeck()
            .map((card: Card) => card.reset());
    }
}

export default GameState;
