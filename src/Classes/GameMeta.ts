class GameMeta
{
    /**
     * The total number of rounds played.
     * The round is incremented when all players made their move.
     *
     * @protected
     * @type {number}
     */
    protected rounds: number;

    /**
     * How many turns players have played.
     * When player must skip their turn, this counter is not incremented.
     *
     * @protected
     * @type {number}
     */
    protected turns: number;

    /**
     * The timestamp of starting the game.
     *
     * @private
     * @type {number}
     */
    private startTimestamp: number;

    /**
     * The timestamp of ending the game.
     *
     * @private
     * @type {number}
     */
    private endTimestamp: number;

    /**
     * Creates game metadata.
     */
    public constructor()
    {
        this.rounds = 0;
        this.turns = 0;
        this.startTimestamp = 0;
        this.endTimestamp = 0;
    }

    /**
     * Returns the game duration.
     * If the game has not finished yet, returns the current duration.
     *
     * @return {number}
     */
    public getGameDuration(): number
    {
        if (this.endTimestamp > 0) {
            return (this.endTimestamp - this.startTimestamp);
        }

        return Date.now() - this.startTimestamp;
    }

    /**
     * Sets the game start timestamp.
     *
     * @param {number} timestamp
     * @return {void}
     */
    public setStartTimestamp(timestamp: number = Date.now()): void
    {
        this.startTimestamp = timestamp;
    }

    /**
     * Sets the game end timestamp.
     *
     * @param {number} timestamp
     * @return {void}
     */
    public setEndTimestamp(timestamp: number = Date.now()): void
    {
        this.endTimestamp = timestamp;
    }
}

export default GameMeta;
