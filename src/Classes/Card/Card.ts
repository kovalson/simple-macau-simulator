import CardType from "../../Enums/CardType";
import Suit from "../../Enums/Suit";
import CardToTypeMapper from "../../Mappers/CardToTypeMapper";
import Rank from "../../Types/Rank";

class Card
{
    /**
     * The card rank.
     * @protected
     * @type {number}
     */
    protected rank: Rank;

    /**
     * The card suit
     * @protected
     * @type {Suit}
     */
    protected suit: Suit;

    /**
     * Creates a new card.
     * @param {Rank} rank
     * @param {Suit} suit
     */
    public constructor(rank: Rank, suit: Suit)
    {
        this.rank = rank;
        this.suit = suit;
    }

    /**
     * Returns the card rank.
     *
     * @return {number}
     */
    public getRank(): Rank
    {
        return this.rank;
    }

    /**
     * Returns the card suit.
     *
     * @return {Suit}
     */
    public getSuit(): Suit
    {
        return this.suit;
    }

    /**
     * Returns the card type.
     *
     * @return {CardType}
     */
    public getType(): CardType
    {
        return CardToTypeMapper.mapCardToType(this);
    }

    /**
     * Checks whether the card has given suit.
     *
     * @param {Suit} suit
     * @return {boolean}
     */
    public hasSuit(suit: Suit): boolean
    {
        return (this.suit === suit);
    }

    /**
     * Returns new base card from this card.
     * This is especially useful when resetting the functional cards that store
     * some additional flags, such as rank-demanding and suit-changing cards.
     *
     * @return {Card}
     */
    public reset(): Card
    {
        return new Card(this.rank, this.suit);
    }

    /**
     * Returns the card string details.
     *
     * @return {string}
     */
    public toString(): string
    {
        return this.rank + " of " + Suit[this.suit];
    }
}

export default Card;
