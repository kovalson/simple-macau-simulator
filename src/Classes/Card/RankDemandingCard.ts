import Suit from "../../Enums/Suit";
import Rank from "../../Types/Rank";
import Card from "./Card";

class RankDemandingCard extends Card
{
    /**
     * Creates a new rank-demanding card from given base card.
     *
     * @param {Card} card
     * @param {Rank} rankDemanded
     * @return {RankDemandingCard}
     */
    public static fromCard(card: Card, rankDemanded: Rank): RankDemandingCard
    {
        return new this(card.getRank(), card.getSuit(), rankDemanded);
    }

    /**
     * The demanded rank.
     *
     * @protected
     * @type {Rank}
     */
    protected rankDemanded: Rank;

    /**
     * Creates a new rank-demanding card.
     *
     * @param {Rank} rank
     * @param {Suit} suit
     * @param {Rank} rankDemanded
     */
    public constructor(rank: Rank, suit: Suit, rankDemanded: Rank)
    {
        super(rank, suit);

        this.rankDemanded = rankDemanded;
    }

    /**
     * Returns the demanded rank.
     *
     * @return {Rank}
     */
    public getRankDemanded(): Rank
    {
        return this.rankDemanded;
    }
}

export default RankDemandingCard;
