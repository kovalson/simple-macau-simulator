import Suit from "../../Enums/Suit";
import Rank from "../../Types/Rank";
import Card from "./Card";

class SuitChangingCard extends Card
{
    /**
     * The current suit of the card.
     *
     * @protected
     * @type {Suit}
     */
    protected currentSuit: Suit;

    /**
     * Creates a new suit-changing card.
     *
     * @override
     * @param {Rank} rank
     * @param {Suit} suit
     * @param {Suit} newSuit
     */
    public constructor(rank: Rank, suit: Suit, newSuit: Suit)
    {
        super(rank, suit);

        this.currentSuit = newSuit;
    }

    /**
     * @override
     * @inheritDoc
     */
    public getSuit(): Suit
    {
        return this.currentSuit;
    }
}

export default SuitChangingCard;
