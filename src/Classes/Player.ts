import CardCollection from "./Collection/CardCollection";
import PlayerCards from "./Collection/PlayerCards";
import GameState from "./State/GameState";
import DefaultStrategy from "./Strategy/DefaultStrategy";
import PlayerStrategy from "./Strategy/PlayerStrategy";

class Player
{
    /**
     * The player's name.
     *
     * @protected
     * @type {string}
     */
    protected name: string;

    /**
     * The player's cards.
     *
     * @protected
     * @type {PlayerCards}
     */
    protected cards: PlayerCards;

    /**
     * The number of turns the player has to skip.
     *
     * @protected
     * @type {number}
     */
    protected turnsToSkip: number;

    /**
     * The player strategy of play.
     *
     * @protected
     * @type {PlayerStrategy}
     */
    protected strategy: PlayerStrategy;

    /**
     * Creates a player.
     */
    public constructor(name: string)
    {
        this.name = name;
        this.cards = new PlayerCards();
        this.turnsToSkip = 0;
        this.strategy = new DefaultStrategy(this);
    }

    /**
     * Returns collection of cards to play for given game state.
     *
     * @param {GameState} gameState
     * @return {CardCollection}
     */
    public getCardsToPlay(gameState: GameState): CardCollection
    {
        return this.strategy.getCardsToPlay(gameState);
    }

    /**
     * Sets player's turns to skip.
     *
     * @param {number} amount
     * @return {void}
     */
    public setTurnsToSkip(amount: number): void
    {
        this.turnsToSkip = amount;
    }

    /**
     * Decreases player's turns to skip by given amount.
     *
     * @param {number} amount
     * @return {void}
     */
    public decreaseTurnsToSkip(amount: number = 1): void
    {
        this.turnsToSkip = Math.max(this.turnsToSkip - amount, 0);
    }

    /**
     * Checks whether the player must skip their turn.
     *
     * @return {boolean}
     */
    public skipsTurn(): boolean
    {
        return (this.turnsToSkip > 0);
    }

    /**
     * Returns the player name.
     *
     * @return {string}
     */
    public getName(): string
    {
        return this.name;
    }

    /**
     * Returns player's cards.
     *
     * @return {PlayerCards}
     */
    public getCards(): PlayerCards
    {
        return this.cards;
    }

    /**
     * Checks whether the player has any cards and is still in game.
     *
     * @return {boolean}
     */
    public isActive(): boolean
    {
        return this.cards.hasItems();
    }
}

export default Player;
