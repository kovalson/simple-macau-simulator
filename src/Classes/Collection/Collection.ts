/**
 * @template T
 */
class Collection<T = any> implements Iterable<T>
{
    /**
     * The array of items.
     *
     * @protected
     * @type {Array<T>>}
     */
    protected items: Array<T>;

    /**
     * Creates a new collection.
     *
     * @param {Array<T> | T} items
     */
    public constructor(items: T | Array<T> = []) {
        this.items = Array.isArray(items) ? items : [items];
    }

    /**
     * Adds new items to the collection.
     *
     * @param {Array<T> | T} items
     * @return {this}
     */
    public add(items: T | Array<T> | Collection<T>): this
    {
        this.items = this.items.concat(items instanceof Collection ? items.items : items);

        return this;
    }

    /**
     * Removes the item from the collection and returns it.
     *
     * @param {number} index
     * @return {T | undefined}
     */
    public removeByIndex(index: number): T | undefined
    {
        const removed: Array<T> = this.items.splice(index, 1);

        return removed[0];
    }

    /**
     * Removes all items from the collection and returns them
     * as a new collection.
     *
     * @return {this}
     */
    public removeAll(): Collection
    {
        const items: Array<T> = this.items;

        this.items = [];

        return new Collection<T>(items);
    }

    /**
     * Returns the first item in the collection.
     *
     * @return {T | undefined}
     */
    public first(): T | undefined
    {
        return this.items[0];
    }

    /**
     * Returns the last item in the collection.
     *
     * @return {T | undefined}
     */
    public last(): T | undefined
    {
        return this.items[this.items.length - 1];
    }

    /**
     * Returns the total number of items in collection.
     *
     * @return {number}
     */
    public count(): number
    {
        return this.items.length;
    }

    /**
     * Checks whether the collection contains an item.
     *
     * @param {T} item
     * @return {boolean}
     */
    public contains(item: T): boolean
    {
        return this.items.includes(item);
    }

    /**
     * Checks whether the collection has any items.
     *
     * @return {boolean}
     */
    public hasItems(): boolean
    {
        return (this.count() > 0);
    }

    /**
     * Maps collection items using given function and returns the collection
     *
     * @param {(item: T, index: number, collection: this) => any} callable
     * @return {this}
     */
    public map(callable: (item: T, index: number, collection: this) => any): this
    {
        this.items = this.items.map((it, idx) => callable(it, idx, this));

        return this;
    }

    /**
     * Iterator method.
     *
     * @returns {Iterator<T>}
     */
    public [Symbol.iterator](): Iterator<T>
    {
        const self: Collection<T> = this;
        let counter: number = 0;

        return {
            next(): IteratorResult<T> {
                if (counter < self.count()) {
                    return {
                        done: false,
                        value: self.items[counter++],
                    };
                } else {
                    return {
                        done: true,
                        value: null,
                    };
                }
            }
        }
    }
}

export default Collection;
