import Player from "../Player";
import Collection from "./Collection";

class PlayerCollection extends Collection<Player>
{
    /**
     * The current player index.
     * @protected
     * @type {number}
     */
    protected currentPlayerIndex: number = 0;

    /**
     * Returns the current player.
     *
     * @return {Player}
     */
    public getCurrentPlayer(): Player
    {
        return this.items[this.currentPlayerIndex];
    }

    /**
     * Moves the index to the next player in the collection.
     *
     * @return {this}
     */
    public nextPlayer(): this
    {
        this.currentPlayerIndex = (this.currentPlayerIndex + 1) % this.items.length;

        return this;
    }

    /**
     * Returns players that have any cards remaining.
     *
     * @return {PlayerCollection}
     */
    public getPlayersWithCards(): PlayerCollection
    {
        return new PlayerCollection(
            this.items.filter((player: Player) => player.getCards().count() > 0)
        );
    }

    /**
     * Returns players that still have cards and play.
     *
     * @return {PlayerCollection}
     */
    public getActivePlayers(): PlayerCollection
    {
        return new PlayerCollection(
            this.items.filter((player: Player) => player.isActive())
        );
    }
}

export default PlayerCollection;
