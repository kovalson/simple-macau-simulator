import Card from "../Card/Card";
import CardCollection from "./CardCollection";

class Stack extends CardCollection
{
    /**
     * Peeks the stack top card.
     *
     * @return {Card}
     */
    public peek(): Card
    {
        return this.last()!;
    }
}

export default Stack;
