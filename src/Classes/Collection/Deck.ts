import Suit from "../../Enums/Suit";
import Rank from "../../Types/Rank";
import Card from "../Card/Card";
import CardCollection from "./CardCollection";

class Deck extends CardCollection
{
    /**
     * Creates a new standard deck of 52-cards.
     *
     * @return {this}
     */
    public init(): this
    {
        this.items = [];

        for (const suit in Suit) {
            if (Suit.hasOwnProperty(suit) && !isNaN(Number(suit))) {
                for (let rank = 2; rank <= 14; rank++) {
                    this.items.push(
                        new Card(rank as Rank, Number(suit) as unknown as number)
                    );
                }
            }
        }

        return this;
    }

    /**
     * Shuffles the deck.
     *
     * @return {this}
     */
    public shuffle(): this
    {
        const shuffled: Array<Card> = [];

        while (this.hasItems()) {
            const randomIndex = Math.floor(Math.random() * this.count());
            const randomItem = this.removeByIndex(randomIndex);

            if (randomItem) {
                shuffled.push(randomItem);
            }
        }

        this.items = shuffled;

        return this;
    }
}

export default Deck;
