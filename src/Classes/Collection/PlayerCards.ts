import Card from "../Card/Card";
import GameState from "../State/GameState";
import CardCollection from "./CardCollection";

class PlayerCards extends CardCollection
{
    /**
     * Returns all cards that can be played during given game state.
     *
     * @param {GameState} state
     * @return {CardCollection}
     */
    public getCardsPlayableForState(state: GameState): CardCollection
    {
        return new CardCollection(
            // Note that we cannot just pass the method directly as the filter function
            // (actually we could) because of the loss of context. It always reminds me of
            // the old joke about Javascript: "Sometimes when I'm writing Javascript I want
            // to throw my hands up and say 'this is bullshit!' but I can never remember
            // what 'this' refers to"
            this.items.filter((card: Card) => state.isCardPlayable(card))
        );
    }
}

export default PlayerCards;
