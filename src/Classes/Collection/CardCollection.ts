import CardType from "../../Enums/CardType";
import Suit from "../../Enums/Suit";
import CardToTypeMapper from "../../Mappers/CardToTypeMapper";
import Rank from "../../Types/Rank";
import Card from "../Card/Card";
import Collection from "./Collection";

class CardCollection extends Collection<Card>
{
    /**
     * Pops a card form the collection and returns it.
     *
     * @return {Card | undefined}
     */
    public draw(): Card | undefined
    {
        return this.items.pop();
    }

    /**
     * Returns type of the first card in collection.
     * In practice, filtered cards in CardCollection object should all be
     * of the same type unless used as other child-class of CardCollection.
     *
     * @return {CardType}
     */
    public getCardsType(): CardType
    {
        return CardToTypeMapper.mapCardToType(
            this.first()!
        );
    }

    /**
     * Sums the values of all cards in the collection.
     *
     * @return {number}
     */
    public getValuesSum(): number
    {
        return this.items.reduce((sum: number, card: Card) => {
            let value: number = card.getRank();

            switch (card.getRank()) {
                case 13:
                    value = 5;
                    break;
                default:
                    break;
            }

            return value;
        }, 0);
    }

    /**
     * Returns the most common suit in the collection.
     * If the collection is empty, returns undefined.
     *
     * @return {Suit}
     */
    public getMostCommonSuit(): Suit | undefined
    {
        if (!this.hasItems()) {
            return undefined;
        }

        const count: number = this.count();
        const suitMap: Map<Suit, number> = new Map();

        let maxSuit: Suit = this.first()!.getSuit();
        let maxCount: number = 1;

        for (let i = 0; i < count; i++) {
            const suit: Suit = this.items[i].getSuit();

            suitMap.set(suit, 1 + (suitMap.get(suit) ?? 0));

            const value: number = suitMap.get(suit)!;

            if (value > maxCount) {
                maxSuit = suit;
                maxCount = value;
            }
        }

        return maxSuit;
    }

    /**
     * Returns the most common rank in the collection.
     * If the collection is empty, returns undefined.
     *
     * @param {Rank | Array<Rank>} except
     * @return {Rank | undefined}
     */
    public getMostCommonRank(except: Rank | Array<Rank>): Rank | undefined
    {
        if (!this.hasItems()) {
            return undefined;
        }

        if (typeof except === "number") {
            except = [except];
        }

        const excludeCollection: Collection<Rank> = new Collection(except);
        const items: Card[] = this.items.filter((card: Card) => !excludeCollection.contains(card.getRank()));
        const count: number = items.length;
        const rankMap: Map<Rank, number> = new Map();

        if (count === 0) {
            return undefined;
        }

        let maxRank: Rank = items[0].getRank();
        let maxCount: number = 1;

        for (let i = 0; i < count; i++) {
            const rank: Rank = items[i].getRank();

            rankMap.set(rank, 1 + (rankMap.get(rank) ?? 0));

            const value: number = rankMap.get(rank)!;

            if (value > maxCount) {
                maxRank = rank;
                maxCount = value;
            }
        }

        return maxRank;
    }

    /**
     * Removes given cards from player's hand and returns itself.
     *
     * @param {CardCollection} cards
     * @return {this}
     */
    public remove(cards: CardCollection): this
    {
        this.items = this.items.filter((card: Card) => !cards.contains(card));

        return this;
    }

    /**
     * Returns string containing all cards details.
     *
     * @return {string}
     */
    public toString(): string
    {
        return this
            .items
            .map(card => card.toString())
            .join(", ");
    }
}

export default CardCollection;
