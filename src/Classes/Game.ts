import config from "../config";
import PlayerFactory from "../Factories/PlayerFactory";
import Card from "./Card/Card";
import Deck from "./Collection/Deck";
import PlayerCollection from "./Collection/PlayerCollection";
import Stack from "./Collection/Stack";
import GameMeta from "./GameMeta";
import GameState from "./State/GameState";
import Regular from "./State/Regular";

class Game
{
    /**
     * The collection of Macau players.
     *
     * @protected
     * @type {PlayerCollection}
     */
    protected players: PlayerCollection;

    /**
     * The game deck of cards.
     * This is where players draw cards from.
     *
     * @protected
     * @type {Deck}
     */
    protected deck: Deck;

    /**
     * The game stack of cards.
     * This is where players play (put) their cards onto.
     *
     * @protected
     * @type {Stack}
     */
    protected stack: Stack;

    /**
     * The current game state.
     *
     * @protected
     * @type {GameState}
     */
    protected state: GameState;

    /**
     * The game metadata.
     *
     * @private
     * @type {GameMeta}
     */
    private meta: GameMeta;

    /**
     * Creates new game.
     */
    public constructor()
    {
        this.players = new PlayerCollection();
        this.deck = new Deck();
        this.stack = new Stack();
        this.state = new Regular(this);
        this.meta = new GameMeta();
    }

    /**
     * Initializes the game.
     *
     * @return {void}
     */
    public init(): void
    {
        this.players.add(
            PlayerFactory.count(config.playersNumber),
        );

        this.deck
            .init()
            .shuffle();

        this.dealCards();

        this.revealFirstCard();

        this.loop();
    }

    /**
     * Restocks the cards in the game deck using the cards remaining
     * on the game stack.
     *
     * @return {void}
     */
    public restockDeck(): void
    {
        // We save the top stack card - it must exist since we only add
        // it in the beginning and never remove it during the game. This
        // card will remain on the game stack to keep track of the context
        const topStackCard: Card = this.stack.draw()!;

        // We transfer all cards from the stack to the deck and we shuffle
        // the deck
        this.deck
            .add(this.stack.removeAll())
            .shuffle();

        // We restore the top stack card
        this.stack.add(topStackCard);
    }

    /**
     * Draws a card from the game deck.
     * If the deck is empty, restocks it with the cards remaining on the stack and
     * shuffles them. If the deck is still empty, returns undefined. The deck may
     * be empty after the restocking when the stack contains only one card (due to
     * large number of players or cards being drawn during the gameplay -  or both).
     * @return {Card | undefined}
     * @private
     */
    public drawCardFromDeck(): Card | undefined
    {
        // We draw the top card from the deck
        let card: Card | undefined = this
            .getDeck()
            .draw();

        if (!card) {
            // We restock the game deck
            this.restockDeck();

            // We try to draw a card again
            card = this
                .getDeck()
                .draw();
        }

        return card;
    }

    /**
     * Returns the players.
     *
     * @return {PlayerCollection}
     */
    public getPlayers(): PlayerCollection
    {
        return this.players;
    }

    /**
     * Returns the current game stack of cards.
     *
     * @return {Stack}
     */
    public getStack(): Stack
    {
        return this.stack;
    }

    /**
     * Returns the current game deck of cards.
     *
     * @return {Deck}
     */
    public getDeck(): Deck
    {
        return this.deck;
    }

    /**
     * Returns the current game state.
     *
     * @return {GameState}
     */
    public getState(): GameState
    {
        return this.state;
    }

    /**
     * Sets new game state.
     *
     * @param {GameState} state
     * @return {void}
     */
    public setState(state: GameState): void
    {
        this.state = state;
    }

    /**
     * The game loop.
     *
     * @protected
     * @return {void}
     */
    protected loop(): void
    {
        this.meta.setStartTimestamp();

        while (!this.isOver()) {
            // How the player's turn is handled depends directly on the current
            // game state, therefore we delegate this task to the state object
            this.state.handlePlayerTurn();

            this.players.nextPlayer();
        }

        this.meta.setEndTimestamp();
    }

    /**
     * Deals cards to players.
     *
     * @protected
     * @return {void}
     */
    protected dealCards(): void
    {
        for (let i = 0; i < config.cardsToDeal; i++) {
            for (const player of this.players) {
                const card: Card | undefined = this.deck.draw();

                // Having no card to deal during the initial dealing is unacceptable
                if (typeof card === "undefined") {
                    throw new Error("There are no cards left to deal. Revise your configuration!");
                }

                player.getCards().add(card);
            }
        }
    }

    /**
     * Reveals the first card and moves it to the top of the stack.
     *
     * @protected
     * @return {void}
     */
    protected revealFirstCard(): void
    {
        const card: Card | undefined = this.deck.draw();

        // Having no first card to reveal is unacceptable
        if (typeof card === "undefined") {
            throw new Error("Cannot reveal first card due to lack of cards. Revise your configuration!");
        }

        this.stack.add(card);
    }

    /**
     * Checks whether the game is over.
     *
     * @protected
     * @return {boolean}
     */
    protected isOver(): boolean
    {
        const playersCount = this
            .getPlayers()
            .getPlayersWithCards()
            .count();

        return (playersCount <= 1);
    }
}

export default Game;
