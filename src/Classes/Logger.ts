import config from "../config";

class Logger
{
    /**
     * Whether the logger is active.
     *
     * @private
     * @type {boolean}
     */
    private static active: boolean = config.logger.active;

    /**
     * Logs given arguments in console.
     *
     * @param {Array} args
     * @return {void}
     */
    public static log(...args: any[]): void
    {
        if (Logger.active) {
            console.log(...args);
        }
    }

    /**
     * Turns the logger on.
     *
     * @return {void}
     */
    public static on(): void
    {
        this.active = true;
    }

    /**
     * Turns the logger off.
     *
     * @return {void}
     */
    public static off(): void
    {
        this.active = false;
    }
}

export default Logger;
