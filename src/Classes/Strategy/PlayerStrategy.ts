import CardCollection from "../Collection/CardCollection";
import Player from "../Player";
import GameState from "../State/GameState";

/**
 * PlayerStrategy class.
 *
 * The player strategy determines their move during their turn. As much as player moves are
 * already limited by the rules of the current game state, some states enable the player to
 * perform different actions.
 */
abstract class PlayerStrategy
{
    /**
     * The player reference.
     *
     * @protected
     * @type {Player}
     */
    protected player: Player;

    /**
     * Creates a new player strategy.
     *
     * @param {Player} player
     */
    public constructor(player: Player)
    {
        this.player = player;
    }

    /**
     * Returns cards to play for given state during player's turn.
     *
     * @param {GameState} gameState
     * @return {CardCollection}
     */
    abstract getCardsToPlay(gameState: GameState): CardCollection;
}

export default PlayerStrategy;
