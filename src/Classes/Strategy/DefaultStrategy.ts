import CardType from "../../Enums/CardType";
import Suit from "../../Enums/Suit";
import Rank from "../../Types/Rank";
import Card from "../Card/Card";
import RankDemandingCard from "../Card/RankDemandingCard";
import SuitChangingCard from "../Card/SuitChangingCard";
import CardCollection from "../Collection/CardCollection";
import GameState from "../State/GameState";
import PlayerStrategy from "./PlayerStrategy";

class DefaultStrategy extends PlayerStrategy
{
    /**
     * @inheritDoc
     */
    public getCardsToPlay(gameState: GameState): CardCollection
    {
        const card: Card | undefined = this
            .player
            .getCards()
            .getCardsPlayableForState(gameState)
            .first();

        const cards = new CardCollection(card ? card : []);

        if (cards.hasItems()) {
            this.player
                .getCards()
                .remove(cards);

            if (cards.getCardsType() === CardType.SuitChanging) {
                this.resolveSuitChange(cards);
            } else if (cards.getCardsType() === CardType.RankDemanding) {
                this.resolveRankDemand(cards);
            }
        }

        return cards;
    }

    /**
     * Handles the suit change.
     *
     * @private
     * @param {CardCollection} cards
     */
    private resolveSuitChange(cards: CardCollection): void
    {
        const newSuit: Suit = this
            .player
            .getCards()
            .getMostCommonSuit()!;

        cards.map((card: Card) => new SuitChangingCard(card.getRank(), card.getSuit(), newSuit));
    }

    /**
     * Handles the rank demand.
     *
     * @private
     * @param {CardCollection} cards
     */
    private resolveRankDemand(cards: CardCollection): void
    {
        const rankDemanded: Rank = this
            .player
            .getCards()
            .getMostCommonRank([11])!;

        cards.map((card: Card) => new RankDemandingCard(card.getRank(), card.getSuit(), rankDemanded));
    }
}

export default DefaultStrategy;
