import RankDemandingCard from "../Classes/Card/RankDemandingCard";
import SuitChangingCard from "../Classes/Card/SuitChangingCard";
import CardCollection from "../Classes/Collection/CardCollection";
import Game from "../Classes/Game";
import Combat from "../Classes/State/Combat";
import GameState from "../Classes/State/GameState";
import RankDemanding from "../Classes/State/RankDemanding";
import SuitChanging from "../Classes/State/SuitChanging";
import TurnSkipping from "../Classes/State/TurnSkipping";
import CardType from "../Enums/CardType";
import Suit from "../Enums/Suit";
import Rank from "../Types/Rank";

class GameStateFactory
{
    /**
     * Returns a new game state based on cards played.
     *
     * @param {Game} game
     * @param {CardCollection} cards
     * @return {GameState}
     */
    public static getStateFromCardsPlayed(game: Game, cards: CardCollection): GameState
    {
        const type: CardType = cards.getCardsType();
        const currentState: GameState = game.getState();

        // If combat cards were played
        if (type === CardType.Combat) {
            const cardsToDraw: number = cards.getValuesSum();

            if (currentState.isType(Combat)) {
                (currentState as Combat).increaseCardsToDraw(cardsToDraw);
            } else {
                return new Combat(game, cardsToDraw);
            }
        }

        // If turn-skipping cards were played
        else if (type === CardType.TurnSkipping) {
            const turnsToSkip: number = cards.count();

            if (currentState.isType(TurnSkipping)) {
                (currentState as TurnSkipping).increaseTurnsToSkip(turnsToSkip);
            } else {
                return new TurnSkipping(game, turnsToSkip);
            }
        }

        // If rank-demanding cards were played
        else if (type === CardType.RankDemanding) {
            const card: RankDemandingCard = cards.last()! as RankDemandingCard;
            const rankDemanded: Rank = card.getRankDemanded();

            return new RankDemanding(game, rankDemanded);
        }

        // If suit-changing cards were played
        else if (type === CardType.SuitChanging) {
            const card: SuitChangingCard = cards.last()! as SuitChangingCard;
            const newSuit: Suit = card.getSuit();

            return new SuitChanging(game, newSuit);
        }

        // Any other card played must be a regular-type card, so we
        // do nothing about that and the current state remains
        return currentState;
    }
}

export default GameStateFactory;
