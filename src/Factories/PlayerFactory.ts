import Player from "../Classes/Player";

class PlayerFactory
{
    /**
     * Creates new player.
     *
     * @return {Player}
     */
    public static make(): Player
    {
        return new Player(PlayerFactory.getRandomName());
    }

    /**
     * Returns array of players of given count.
     *
     * @param {number} n
     * @return {Array<Player>>}
     */
    public static count(n: number): Array<Player>
    {
        return Array.from(
            { length: n },
            this.make
        );
    }

    /**
     * Returns a random name for the player.
     *
     * @private
     * @return {string}
     */
    private static getRandomName(): string
    {
        const names: string[] = [
            "Alfred",
            "Josh",
            "John",
            "Mike",
            "Frank",
            "Buzz",
            "Andrew",
            "Albert",
            "Bert",
            "Bernard",
            "Patrick",
            "James",
            "Jack",
            "Anthony",
            "Eric",
            "Alec",
            "Sebastian",
        ];

        const randomIndex = Math.floor(Math.random() * names.length);

        return names[randomIndex];
    }
}

export default PlayerFactory;
