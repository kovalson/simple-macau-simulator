const config = Object.freeze({
    playersNumber: 4,
    cardsToDeal: 5,
    logger: {
        active: true,
    },
});

export default config;
