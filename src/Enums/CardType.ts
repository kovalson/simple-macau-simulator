enum CardType
{
    Regular,
    Combat,
    TurnSkipping,
    RankDemanding,
    SuitChanging,
}

export default CardType;
