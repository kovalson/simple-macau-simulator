import Game from "./Classes/Game";
import Logger from "./Classes/Logger";

Logger.off();

const game = new Game();
game.init();
